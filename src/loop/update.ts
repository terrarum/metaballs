import { Circle } from '@/interfaces/circle';
import { CanvasMeta } from '@/interfaces/canvasMeta';

export default function update(circles: Circle[], canvas: CanvasMeta, dt: number) {
    if (dt > 2) {
        console.log(`Large Delta Time: ${dt}. Constrained to 1.`);
        dt = 1;
    }
    circles.forEach(circle => {
        circle.x += circle.vx * dt;
        circle.y += circle.vy * dt;

        if (circle.x - circle.r < 0) {
            circle.vx = Math.abs(circle.vx);
        }
        if (circle.x + circle.r > canvas.width) {
            circle.vx = -Math.abs(circle.vx);
        }
        if (circle.y - circle.r < 0) {
            circle.vy = Math.abs(circle.vy);
        }
        if (circle.y + circle.r > canvas.height) {
            circle.vy = -Math.abs(circle.vy);
        }
    });
}
