import store from '@/store';
import { createCanvas, createLoop } from 'simple-gameloop';
import { CanvasMeta } from '@/interfaces/canvasMeta';
import sps from '@/utils/sps';

function createCircles() {
    const circle = store.getters.circle;
    for (let i = 0; i < circle.count; i++) {
        store.commit('addCircle');
    }
}

function initCanvas() {
    const canvas = store.getters.canvas;
    const canvasObject = createCanvas({
        containerSelector: '.canvas-container',
        classes: 'canvas',
        width: canvas.width,
        height: canvas.height,
    });
    store.commit('setCanvasObject', canvasObject);
}

function update(dt: number) {
    const isRunning = store.getters.isRunning;

    if (isRunning) {
        store.commit('update', dt);
    }
}

let fpsMeter: sps | null = null;

function render() {
    if (!fpsMeter) {
        throw new Error('FPS meter missing');
    }
    fpsMeter.begin();
    const renderer = store.getters.currentRenderer;
    const canvas: CanvasMeta = store.getters.canvas;
    const ctx = canvas.context;
    ctx?.clearRect(0, 0, canvas.width, canvas.height);

    renderer.render();
    fpsMeter.end();
}

export default function initLoop(fps: sps) {
    store.commit('reset');
    fpsMeter = fps;
    createCircles();
    initCanvas();

    createLoop({
        update,
        render,
    });
}
