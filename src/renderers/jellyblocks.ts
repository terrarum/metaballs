import store from '@/store';
import { framePosition, pointInCircleProx } from '@/utils/utils';
import { Circle } from '@/interfaces/circle';
import { Color } from 'chroma-js';

interface Square {
    x: number;
    y: number;
    proximity: number;
    circles: {
        circle: Circle;
        proximity: number;
    }[];
    color: Color | null;
}

let squares: Square[] = [];

function init() {
    squares = [];
    const canvas = store.getters.canvas;
    const res = canvas.resolution;
    for (let x = 0; x < canvas.width + res; x += res) {
        for (let y = 0; y < canvas.height + res; y += res) {
            squares.push({
                x,
                y,
                proximity: 0,
                circles: [],
                color: null,
            });
        }
    }
}

function render() {
    const canvas = store.getters.canvas;
    const circles = store.getters.circles;
    const ctx = canvas?.context;
    const squareRenderSize = store.getters.squareRenderSize;
    ctx.fillStyle = '#000000';

    squares.forEach(square => {
        square.proximity = 0;
        square.circles = [];

        circles.forEach((circle: Circle) => {
            const proximity = pointInCircleProx(square, circle);
            square.proximity += proximity;

            square.circles.push({
                circle,
                proximity,
            });
        });

        square.circles.sort((a, b) => {
            return b.proximity - a.proximity;
        })

        square.color = square.circles[0].circle.color;

        const frame = framePosition(square, canvas.resolution);
        if (square.proximity >= 1) {
            ctx.beginPath();
            ctx.fillStyle = square.color.hex();
            ctx.fillRect(frame.x, frame.y, squareRenderSize, squareRenderSize);
        }
    });
}

export default {
    name: 'Jelly Blocks',
    init,
    render,
};
