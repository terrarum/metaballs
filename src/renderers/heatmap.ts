import store from '@/store';
import { clamp } from 'lodash';
import { framePosition, pointInCircleProx } from '@/utils/utils';
import { Circle } from '@/interfaces/circle';
import chroma, { Color } from 'chroma-js';
import { easeOutExponential } from '@/utils/easing';

interface Square {
    x: number;
    y: number;
    proximity: number;
    color: Color | null;
}

let squares: Square[] = [];

const maxTemp = 30000;

function heatmapValue(proximity: number): number {
    let val = proximity / 100;
    val = clamp(val, 0, 1);
    val = easeOutExponential(val);
    val = val * maxTemp;
    return clamp(val, 200, maxTemp) + 500;
}

function init() {
    squares = [];
    const canvas = store.getters.canvas;
    const res = canvas.resolution;
    for (let x = 0; x < canvas.width + res; x += res) {
        for (let y = 0; y < canvas.height + res; y += res) {
            squares.push({
                x,
                y,
                proximity: 0,
                color: null,
            });
        }
    }
}

function render() {
    const canvas = store.getters.canvas;
    const circles = store.getters.circles;
    const ctx = canvas?.context;
    const squareRenderSize = store.getters.squareRenderSize;
    ctx.fillStyle = '#000000';

    squares.forEach(square => {
        square.proximity = 0;

        circles.forEach((circle: Circle) => {
            const proximity = pointInCircleProx(square, circle);
            square.proximity += proximity;
        });

        square.color = chroma.temperature(heatmapValue(square.proximity));

        const frame = framePosition(square, canvas.resolution);
        ctx.beginPath();
        ctx.fillStyle = square.color.hex();
        ctx.fillRect(frame.x, frame.y, squareRenderSize, squareRenderSize);
    });
}

export default {
    name: 'Heat Map',
    init,
    render,
};
