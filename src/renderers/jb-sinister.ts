import store from '@/store';
import { framePosition, pointInAllCircles } from '@/utils/utils';

interface Square {
    x: number;
    y: number;
}

let squares: Square[] = [];

function init() {
    squares = [];
    const canvas = store.getters.canvas;
    const res = canvas.resolution;
    for (let x = 0; x < canvas.width + res; x += res) {
        for (let y = 0; y < canvas.height + res; y += res) {
            squares.push({ x, y });
        }
    }
}

function proxToRGB(circleProximity: number, gradientRange: number) {
    const ratio = (circleProximity - gradientRange) / (1 - gradientRange);
    return 255 - ratio * 255;
}

function render() {
    const canvas = store.getters.canvas;
    const circles = store.getters.circles;
    const ctx = canvas?.context;
    const squareRenderSize = store.getters.squareRenderSize;
    ctx.fillStyle = '#000000';

    squares.forEach(square => {
        const frame = framePosition(square, canvas.resolution);

        const circleProximity = pointInAllCircles(square, circles);


        ctx.beginPath();
        if (circleProximity >= 2) {
            ctx.fillStyle = '#000000';
        }
        else if (circleProximity >= 0.1 && circleProximity < 2) {
            const shade = proxToRGB(circleProximity, 0.1);
            ctx.fillStyle = `rgb(${shade}, ${shade}, ${shade})`;
        }
        else {
            ctx.fillStyle = '#ffffff';
        }

        ctx.fillRect(frame.x, frame.y, squareRenderSize, squareRenderSize);
    })
}

export default {
    name: 'Sinister',
    init,
    render,
}
