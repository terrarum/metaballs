import store from '@/store';
import { Circle } from '@/interfaces/circle';

function render() {
    const canvas = store.getters.canvas;
    const circles = store.getters.circles;
    const ctx = canvas?.context;

    circles.forEach((circle: Circle) => {
        ctx.beginPath();
        ctx.fillStyle = circle.color.hex();
        ctx.arc(circle.x, circle.y, circle.r, 0, 2 * Math.PI);
        ctx.fill();
        ctx.closePath();
    });
}

export default {
    name: 'Circles',
    render,
}
