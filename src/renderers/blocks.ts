import store from '@/store';
import { Circle } from '@/interfaces/circle';
import { pointInCircle } from '@/utils/utils';

interface Square {
    x: number;
    y: number;
}

let squares: Square[] = [];

function init() {
    squares = [];
    const canvas = store.getters.canvas;
    const res = canvas.resolution;
    for (let x = 0; x < canvas.width + res; x += res) {
        for (let y = 0; y < canvas.height + res; y += res) {
            squares.push({ x, y });
        }
    }
}

function render() {
    const canvas = store.getters.canvas;
    const circles = store.getters.circles;
    const squareRenderSize = store.getters.squareRenderSize;
    const ctx = canvas?.context;

    squares.forEach(square => {
        let frameX = square.x - canvas.resolution / 2;
        let frameY = square.y - canvas.resolution / 2;
        if (canvas.resolution % 2) {
            frameX += 0.5;
            frameY += 0.5;
        }

        circles.forEach((circle: Circle) => {
            if (pointInCircle(square, circle)) {
                ctx.beginPath();
                ctx.fillStyle = circle.color.hex();
                ctx.fillRect(frameX, frameY, squareRenderSize, squareRenderSize);
            }
        })
    })
}

export default {
    name: 'Blocks',
    init,
    render,
}
