import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import { Circle } from '@/interfaces/circle';
import { State } from '@/interfaces/state';
import { random } from 'lodash';
import chroma from 'chroma-js';
import update from '@/loop/update';

import circles from '@/renderers/circles';
import blocks from '@/renderers/blocks';
import jellyblocks from '@/renderers/jellyblocks';
import jellyblend from '@/renderers/jellyblend';
import jbSinister from '@/renderers/jb-sinister';
import { Renderer } from '@/interfaces/renderer';
import heatmap from '@/renderers/heatmap';

function getState(): State {
    return {
        running: true,
        tick: false,
        renderers: [
            circles,
            blocks,
            jellyblocks,
            jellyblend,
            heatmap,
            jbSinister,
        ],
        currentRenderer: circles,
        canvas: {
            resolution: 10,
            width: 600,
            height: 500,
        },
        squareRenderSize: 10,
        circle: {
            count: 10,
            minRadius: 20,
            maxRadius: 50,
            speed: 50,
        },
        circles: [],
    };
}

export default new Vuex.Store({
    state: getState,
    mutations: {
        reset(state) {
            state.circles = [];
            state.canvas.context = undefined;
            state.canvas.element = undefined;
        },
        playPause(state) {
            state.running = !state.running;
        },
        update(state, dt) {
            update(state.circles, state.canvas, dt);
        },
        setCanvasObject(state, canvasObject) {
            state.canvas.element = canvasObject.element;
            state.canvas.context = canvasObject.context;
        },
        setRenderer(state, renderer: Renderer) {
            if (renderer.init !== undefined) {
                renderer.init();
            }
            state.currentRenderer = renderer;
        },
        addCircle(state) {
            const circle: Circle = {
                x: random(state.circle.maxRadius, state.canvas.width - state.circle.maxRadius),
                y: random(state.circle.maxRadius, state.canvas.height - state.circle.maxRadius),
                r: random(state.circle.minRadius, state.circle.maxRadius),
                vx: random(-state.circle.speed, state.circle.speed),
                vy: random(-state.circle.speed, state.circle.speed),
                color: chroma.random(),
            };

            state.circles.push(circle);
        },
        removeCircle(state) {
            if (state.circles.length === 0) return;
            state.circles.splice(random(0, state.circles.length - 1), 1);
        },
        increaseResolution(state) {
            const resolution = state.canvas.resolution + 1;
            if (state.squareRenderSize === state.canvas.resolution) {
                state.squareRenderSize = resolution;
            }
            state.canvas.resolution = resolution;
        },
        decreaseResolution(state) {
            if (state.canvas.resolution === 1) {
                return;
            }
            state.canvas.resolution -= 1;
            if (state.canvas.resolution < state.squareRenderSize) {
                state.squareRenderSize = state.canvas.resolution;
            }
        },
        increaseSquareRenderSize(state) {
            const squareRenderSize = state.squareRenderSize + 1;
            if (squareRenderSize > state.canvas.resolution) {
                return;
            }
            state.squareRenderSize = squareRenderSize;
        },
        decreaseSquareRenderSize(state) {
            if (state.squareRenderSize === 1) {
                return;
            }
            state.squareRenderSize -= 1;
        },
        setColours(state, color: string | null) {
            state.circles.forEach(circle => {
                circle.color = color !== null ? chroma(color) : chroma.random();
            })
        }
    },
    actions: {},
    modules: {},
    getters: {
        isRunning: state => state.running,
        tick: state => state.tick,
        canvas: state => state.canvas,
        circle: state => state.circle,
        circles: state => state.circles,
        renderers: state => state.renderers,
        currentRenderer: state => state.currentRenderer,
        circleCount: state => state.circles.length,
        resolution: state => state.canvas.resolution,
        squareRenderSize: state => state.squareRenderSize,
    }
});
