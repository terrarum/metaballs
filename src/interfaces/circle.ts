import { Color } from 'chroma-js';

export interface Circle {
    x: number;
    y: number;
    r: number;
    vx: number;
    vy: number;
    color: Color;
}
