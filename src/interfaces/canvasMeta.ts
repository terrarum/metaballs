export interface CanvasMeta {
    width: number;
    height: number;
    resolution: number;
    element?: HTMLCanvasElement;
    context?: CanvasRenderingContext2D;
}
