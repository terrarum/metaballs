export interface Renderer {
    name: string;
    init?: () => void;
    update?: (dt: number) => void;
    render: () => void;
}
