export interface Input {
    description: string;
    type: string;
    value: number;
}
