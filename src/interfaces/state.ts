import { CanvasMeta } from '@/interfaces/canvasMeta';
import { Input } from '@/interfaces/controls';
import { Circle } from '@/interfaces/circle';
import { Renderer } from '@/interfaces/renderer';

export interface State {
    running: boolean;
    tick: boolean;
    renderers: Renderer[];
    currentRenderer: Renderer;
    canvas: CanvasMeta;
    squareRenderSize: number;
    circle: {
        count: number;
        minRadius: number;
        maxRadius: number;
        speed: number;
    };
    inputs?: Input[];
    circles: Circle[];
}
