import { Circle } from '@/interfaces/circle';

export function pointInCircleProx(point: { x: number; y: number }, circle: Circle) {
    const distance = (Math.pow(point.x - circle.x, 2) + Math.pow(point.y - circle.y, 2));
    const radius = Math.pow(circle.r, 2);
    return radius / distance;
}

export function pointInCircle(point: { x: number; y: number }, circle: Circle) {
    return pointInCircleProx(point, circle) >= 1;
}

export function pointInAllCircles(point: { x: number; y: number }, circles: Circle[]) {
    let val = 0;
    circles.forEach(circle => {
        val += pointInCircleProx(point, circle);
    });
    return val;
}

export function framePosition(square: {x: number; y: number}, resolution: number) {
    let frameX = square.x - resolution / 2;
    let frameY = square.y - resolution / 2;
    frameX += resolution % 2 ? 0.5 : 0;
    frameY += resolution % 2 ? 0.5 : 0;
    return {
        x: frameX,
        y: frameY,
    }
}
