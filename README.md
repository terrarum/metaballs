# Metaballs

I'm working my way through this: http://jamie-wong.com/2014/08/19/metaballs-and-marching-squares/

## To Do
- [X] FPS counter
- [X] Input to change resolution
- [X] Input to change number of circles
- [ ] Modify render colour and gradient somehow?
- [ ] Stop when out of focus
- [ ] Allow circles to wrap instead of bounce
- [ ] Allow circles to collide


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
